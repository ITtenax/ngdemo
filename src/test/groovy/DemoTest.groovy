import org.testng.SkipException
import org.testng.annotations.Test
import org.uncommons.reportng.exceptions.EnvironmentException
import org.uncommons.reportng.models.Bug

/**
 * Created by Valentyn on 6/15/2016.
 */

class DemoTest extends ReportNgListener {
    @Test(invocationCount = 2)
    void bugTest() {
        Bug.register("CWR-17276", RuntimeException.class);
        throw new RuntimeException();
    }

    @Test(invocationCount = 5)
    void passTest() {

    }

    @Test(invocationCount = 2)
    void skippedTest() {
        throw new SkipException("");
    }

    @Test(invocationCount = 4)
    void failTest() {
        throw new RuntimeException();
    }

    @Test(invocationCount = 2)
    void envExceptionTest() {
        throw new EnvironmentException("Server unavailable");
    }

    @Test(invocationCount = 1)
    void closedBugTest() {
        Bug.register("CWR-13709", RuntimeException.class);
        throw new RuntimeException();
    }

    @Test(invocationCount = 3)
    void taskTest() {
        Bug.register("CWR-17882", RuntimeException.class);
        throw new RuntimeException();
    }
}
