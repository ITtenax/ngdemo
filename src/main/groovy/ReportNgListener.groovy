import org.testng.annotations.Listeners
import org.uncommons.reportng.listeners.ExceptionListener
import org.uncommons.reportng.listeners.ReporterListener

/**
 * Created by Valentyn on 6/15/2016.
 */
@Listeners([ReporterListener.class, ExceptionListener.class])
class ReportNgListener {
    static {
        System.setProperty("org.uncommons.reportng.local-report-directory", "./report");
        System.setProperty("org.uncommons.reportng.escape-output", "false");
        System.setProperty("org.uncommons.reportng.cq-jira-url", "https://ctc-customs.atlassian.net");
        System.setProperty("org.uncommons.reportng.jira-login", "Dmitry Lazarev");
        System.setProperty("org.uncommons.reportng.jira-pass", "12345678");
    }
}
